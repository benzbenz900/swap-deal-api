# Strapi application

A quick description of your strapi application



http://localhost:1337/graphql

query {
  products {
    NameProduct
    ImageProduct {
      formats
    }
    _id
  }
}

{
    "data": {
        "products": [
            {
                "NameProduct": "กะหล่ำปลี",
                "ImageProduct": [
                    {
                        "formats": {
                            "thumbnail": {
                                "name": "thumbnail_1.jpg",
                                "hash": "thumbnail_1_ba596f17e9",
                                "ext": ".jpg",
                                "mime": "image/jpeg",
                                "width": 245,
                                "height": 126,
                                "size": 9.71,
                                "path": null,
                                "url": "/uploads/thumbnail_1_ba596f17e9.jpg"
                            },
                            "medium": {
                                "name": "medium_1.jpg",
                                "hash": "medium_1_ba596f17e9",
                                "ext": ".jpg",
                                "mime": "image/jpeg",
                                "width": 750,
                                "height": 386,
                                "size": 52.95,
                                "path": null,
                                "url": "/uploads/medium_1_ba596f17e9.jpg"
                            },
                            "small": {
                                "name": "small_1.jpg",
                                "hash": "small_1_ba596f17e9",
                                "ext": ".jpg",
                                "mime": "image/jpeg",
                                "width": 500,
                                "height": 258,
                                "size": 29.95,
                                "path": null,
                                "url": "/uploads/small_1_ba596f17e9.jpg"
                            }
                        }
                    }
                ],
                "_id": "5f6a5c9409805c0adcdf506b"
            },
            {
                "NameProduct": "หมูแดดเดียว",
                "ImageProduct": [
                    {
                        "formats": {
                            "thumbnail": {
                                "name": "thumbnail_2.jpg",
                                "hash": "thumbnail_2_abaf8288e0",
                                "ext": ".jpg",
                                "mime": "image/jpeg",
                                "width": 175,
                                "height": 156,
                                "size": 10.58,
                                "path": null,
                                "url": "/uploads/thumbnail_2_abaf8288e0.jpg"
                            },
                            "large": {
                                "name": "large_2.jpg",
                                "hash": "large_2_abaf8288e0",
                                "ext": ".jpg",
                                "mime": "image/jpeg",
                                "width": 1000,
                                "height": 892,
                                "size": 196.65,
                                "path": null,
                                "url": "/uploads/large_2_abaf8288e0.jpg"
                            },
                            "medium": {
                                "name": "medium_2.jpg",
                                "hash": "medium_2_abaf8288e0",
                                "ext": ".jpg",
                                "mime": "image/jpeg",
                                "width": 750,
                                "height": 669,
                                "size": 123.89,
                                "path": null,
                                "url": "/uploads/medium_2_abaf8288e0.jpg"
                            },
                            "small": {
                                "name": "small_2.jpg",
                                "hash": "small_2_abaf8288e0",
                                "ext": ".jpg",
                                "mime": "image/jpeg",
                                "width": 500,
                                "height": 446,
                                "size": 63.18,
                                "path": null,
                                "url": "/uploads/small_2_abaf8288e0.jpg"
                            }
                        }
                    }
                ],
                "_id": "5f6b7aba9aa5fd1b4815adc3"
            }
        ]
    }
}